$(document).ready(function () {
    var body = $('body');
    var html = $('html');

    var menuOpenFlag = false;
    body.on('click','.closed.mobile-menu-link',function(){
        if (menuOpenFlag) return false;
        menuOpenFlag = true;
        var obj = $(this);
        obj.addClass('open');
        $('.mobile-header-menu-wrap').addClass('animated slideInLeft');
        $('.mobile-header-menu-wrap').addClass('open-menu');
        obj.removeClass('closed');
        setTimeout(function(){
            obj.addClass('in-animation');
            $('.mobile-header-menu ul li').each(function(){
                var obj = $(this);
                setTimeout(function(){
                    obj.show().addClass('animated slideInLeft');
                },obj.index()*70);
            });
            menuOpenFlag = false;
        },300);
    });
    body.on('click','.open.mobile-menu-link',function(){
        if (menuOpenFlag) return false;
        menuOpenFlag = true;
        var obj = $('.open.mobile-menu-link');
        obj.removeClass('in-animation');
        setTimeout(function(){
            obj.removeClass('open');
            obj.addClass('closed');
            menuOpenFlag = false;
            $('.mobile-header-menu ul li').hide().removeClass('animated slideInLeft');
            $('.mobile-header-menu-wrap').removeClass('animated slideInLeft');
            $('.mobile-header-menu-wrap').removeClass('open-menu');
        },300);
    });
});